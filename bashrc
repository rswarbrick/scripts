# Link to this and use it as .bashrc

# This first part of the file is cribbed from the default you get for
# a new Debian user

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Bash setup: Append to history file; don't put duplicate lines or
# lines starting with space in the history; set number of commands to
# remember in history file.
shopt -s histappend
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

# Bash: check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Use tput to decide whether we have colour
if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	color_prompt=yes
else
	color_prompt=
fi

# Set prompt
if [ "$color_prompt" = yes ]; then
    PS1='\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='\u@\h:\w\$ '
fi

# Unset the variables we used above
unset color_prompt

# Colours in ls
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
fi

# Aliases
alias ll='ls -l'
alias ..='cd ..'

# Enable bash completion
if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

# Add scripts to path
scripts_dir="$(dirname "$(readlink -f $BASH_SOURCE)")"
export PATH="$PATH:$scripts_dir"

# DBK integration
s() { dbk set "$@"; }
i() { local dir="$(dbk get "$@")" && cd $dir; }

# Tell less to store its history in $XDG_DATA_HOME/less/lesshst
less_data_home="${XDG_DATA_HOME:-$HOME/.local/share}"/less
mkdir -p "$less_data_home"
export LESSHISTFILE="$less_data_home/lesshst"

# Add ~/.local/bin to PATH. For example, code installed by pip goes
# there
export PATH="$PATH:$HOME/.local/bin"

# Local Variables:
# mode: sh
# sh-shell: bash
# End:
