scripts := vpd-to-fst dbk

.PHONY: all
all: mypy

.mypy:
	mkdir -p .mypy

mypy-stamps := $(foreach s,$(scripts),.mypy/$(s).good)

$(mypy-stamps): .mypy/%.good: % | .mypy
	mypy --strict $< && touch $@

.PHONY: mypy
mypy: $(mypy-stamps)
