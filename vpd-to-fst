#!/usr/bin/env python3

'''A converter that transforms VPD files into FST files

The VPD file format is supported by VCS and is what you get by default
if you run "dump -add ..." in your TCL script. FST is supported by
GTKWave and is reasonably efficient.

'''

import argparse
import os
import subprocess
import sys
import tempfile
import time

from typing import List


def kill_procs(procs: List['subprocess.Popen[bytes]']) -> None:
    '''Terminate/Kill each running process in procs'''
    for p in procs:
        if p.poll() is None:
            p.terminate()

    have_waited = False
    for p in procs:
        if p.poll() is not None:
            continue

        if not have_waited:
            # Give processes 500ms to tidy up.
            time.sleep(0.5)
            p.poll()
            have_waited = True

        if p.returncode is None:
            p.kill()


def wait_procs(procs: List['subprocess.Popen[bytes]']) -> int:
    '''Wait for each process in procs.

    We use a timeout with loop so that if the second process dies, we don't
    hang forever waiting for the first. Returns a retcode which is zero if all
    processes succeeded or otherwise is the first non-zero retcode we saw.

    If we return 0, all processes have finished. Otherwise, one or more
    processes may still be running.

    '''
    while True:
        all_done = True
        for p in procs:
            try:
                retcode = p.wait(0.25)
                if retcode != 0:
                    kill_procs(procs)
                    return retcode
            except subprocess.TimeoutExpired:
                all_done = False
                pass

        if all_done:
            break

    return 0


def main() -> int:
    parser = argparse.ArgumentParser()
    parser.add_argument('infile')
    parser.add_argument('outfile', nargs='?')

    args = parser.parse_args()

    if args.outfile is None:
        base, ext = os.path.splitext(args.infile)
        if ext != '.vpd':
            args.outfile = args.infile + '.fst'
        else:
            args.outfile = base + '.fst'

    with tempfile.TemporaryDirectory() as tmpdir:
        pipe_path = os.path.join(tmpdir, 'vcd.pipe')
        os.mkfifo(pipe_path)

        procs: List['subprocess.Popen[bytes]'] = []
        try:
            procs.append(subprocess.Popen(['vpd2vcd', args.infile, pipe_path],
                                          stdin=subprocess.DEVNULL,
                                          stdout=subprocess.DEVNULL))
            procs.append(subprocess.Popen(['vcd2fst', pipe_path, args.outfile],
                                          stdin=subprocess.DEVNULL,
                                          stdout=subprocess.DEVNULL))

            return wait_procs(procs)
        finally:
            kill_procs(procs)


if __name__ == '__main__':
    try:
        sys.exit(main())
    except RuntimeError as err:
        sys.stderr.write('Error: {}\n'.format(err))
