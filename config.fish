# Fish configuration file
#
# Either symlink .config/fish/config.fish to this file or source this
# file from there after doing anything machine-specific.


## Use ccache
echo $PATH | grep 'ccache' >/dev/null
if test $status -eq 1
 set PATH /usr/lib/ccache $PATH
end

# Add the scripts directory to PATH
command -v dbk >/dev/null
if test $status -ne 0
 set PATH $PATH (dirname (readlink -f (status -f)))
end

## Use a sensible browser to display html (eg "help")
set -x BROWSER x-www-browser

## Editor (mostly for emergency git).
set -x EDITOR emacsclient

# A handy repeat function (do this thing again and again)
function repeat
  while true
    eval $argv
    read -p "echo -e '\n\nAgain? [Y/n]  '" ans
    if test "$ans" = n
     break
    end
 end
end

# Add distcc to path, just after ccache
function distpath
  set -l distccpath /usr/lib/distcc

  # If distcc is already on the path, nothing to do
  if contains $distccpath $PATH
    return
  end

  set -l newpath
  set -l found 0

  # Try to add distcc just after ccache (so we fill the local cache
  # with results from the other machines)
  for x in $PATH
    set -a newpath $x
    if string match -q "*/ccache" $x
      set -a newpath $distccpath
      set found 1
    end
  end

  # If we didn't find ccache, add distcc at the start (to override
  # things like /usr/bin/cc)
  if test $found = 0
    set -p newpath $distccpath
  end

  set -x PATH $newpath
end

distpath

# DBK integration
function i
  set -l dir (dbk get $argv); and cd $dir
end

function s
  dbk set $argv
end

# "open"
function o
  xdg-open $argv
end

if set -q XDG_DATA_HOME
  set fish_data_home $XDG_DATA_HOME
else
  set fish_data_home $HOME/.local/share
end

# Tell less where to store its history
set -x LESSHISTFILE $fish_data_home/less/lesshst

# Add .local/bin to path
set PATH $PATH $HOME/.local/bin


# Local Variables:
# mode: sh
# sh-shell: fish
# End:
