{
  description = "Rupert's machines";


  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    lowrisc-it = {
      url = "git+ssh://git@github.com/lowRISC/lowrisc-it";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    lowrisc-it,
  }:
    {
      nixosConfigurations = let
        system = "x86_64-linux";
        specialArgs = {
          inherit self nixpkgs lowrisc-it;
        };
      in {
        rjs-dab = nixpkgs.lib.nixosSystem {
          inherit system specialArgs;
          modules = [
            ./mod/rjs-dab.nix
            ./mod/workstation.nix
#           ./mod/lowrisc-it.nix
          ];
        };
      };
    };
}
