{
  self,
  pkgs,
  nixpkgs,
  lowrisc-it,
  ...
}: {
  # lowrisc stuff
  imports = [
    lowrisc-it.nixosModules.lowrisc
  ];
  lowrisc = {
    identity = "rswarbrick@lowrisc.org";
    network = true;
    toolnas.enable = true;
    usePublicCache = true;
  };
  nix.registry.lowrisc-it.flake = lowrisc-it;
}
